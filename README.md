# data

This is the repository intended for storing the data used in the `pca` and `neural` repositories in the `spectra-aiml` group. As the data is proprietary, we opted to only include a single spectrum in both the `lithium/interpolated` and `lithium/measured` folders. The included `.csv` files in these folders are intended to be demonstrational to show the expected format of the data files used to train these models.

## Expected Data Format

### Spectra Files

Spectra files are expected to be `CSV` files, formatted in the following manner:

| Wavenumber (cm^-1) | 1M    | 2M    | ...   |
| ------------------ | ----- | ----- | ----- |
| 1                  | 0.1   | 0.2   | ...   |
| 2                  | 0.2   | 0.3   | ...   |
| 3                  | 0.3   | 0.4   | ...   |
| ...                | ...   | ...   | ...   |

### Mole Fraction Files

Mole fraction files are expected to be `CSV` files, formatted in the following manner:

| Molarity (mol/L) | Mole % Component1 | Mole % Component2 | Mole % Component3 |
| ---------------- | ----------------- | ----------------- | ----------------- |
| 1.0              | 0.1               | 0.2               | 0.7               |
| 2.0              | 0.2               | 0.3               | 0.5               |
| 3.0              | 0.3               | 0.4               | 0.3               |
| ...              | ...               | ...               | ...               |

## Inference Data Format

When using a trained model to inference on or predict from a dataset, it is expected that the inference dataset matches the format of the spectra file format above. The column titles depicting molarity are not used but are expected to be there, they can be integer labels.